package br.com.alura.forum.service;

import br.com.alura.forum.controller.dto.TopicoDTO;
import br.com.alura.forum.controller.form.AtualizacaoTopicoForm;
import br.com.alura.forum.controller.form.NovoTopicoForm;
import br.com.alura.forum.model.Curso;
import br.com.alura.forum.model.Topico;
import br.com.alura.forum.model.Usuario;
import br.com.alura.forum.repository.CursoRepository;
import br.com.alura.forum.repository.TopicoRepository;
import br.com.alura.forum.repository.UsuarioRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class TopicoService {

    @Autowired
    private TopicoRepository topicoRepository;

    @Autowired
    private CursoRepository cursoRepository;

    @Autowired
    private UsuarioRepository usuarioRepository;

    private ModelMapper modelMapper = new ModelMapper();

    @Cacheable(value = "listaDeTopicos")
    public Page<TopicoDTO> buscaTodos(String nomeCurso, Pageable paginacao) {

        Page<Topico> todos;

        if (nomeCurso != null) {
            todos = topicoRepository.findByCursoNome(nomeCurso,paginacao);
        } else {
            todos = topicoRepository.findAll(paginacao);
        }
        return todos.map(it-> modelMapper.map(it, TopicoDTO.class));
    }

    @CacheEvict(value = "listaDeTopicos",allEntries = true)
    public ResponseEntity<TopicoDTO> salvarTopico(NovoTopicoForm form, UriComponentsBuilder uriBuilder) {
        Usuario logado = usuarioRepository.getOne(1l);
        Curso curso = cursoRepository.findByNome(form.getNomeCurso());
        Topico novo = Topico.builder().curso(curso).titulo(form.getTitulo()).mensagem(form.getMensagem()).autor(logado).build();
        topicoRepository.save(novo);

        URI uri = uriBuilder.path("/topicos/{id}").buildAndExpand(novo.getId()).toUri();
        return ResponseEntity.created(uri).body(modelMapper.map(novo, TopicoDTO.class));
    }

    public ResponseEntity<TopicoDTO> buscarPorId(Long id) {
        Optional<Topico> detalhado = topicoRepository.findById(id);
        if (!detalhado.isPresent()) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(modelMapper.map(detalhado.get(), TopicoDTO.class));
    }

    @CacheEvict(value = "listaDeTopicos",allEntries = true)
    public ResponseEntity<?> atualiza(Long id, @Valid AtualizacaoTopicoForm form) {
        Optional<Topico> topico = topicoRepository.findById(id);
        if (!topico.isPresent()) {
            return ResponseEntity.notFound().build();
        }
        Topico editado = topico.get();
        editado.atualizarInformacoes(form.getTitulo(),form.getMensagem(),cursoRepository.findByNome(form.getNomeCurso()));

        topicoRepository.save(editado);
        return ResponseEntity.ok().build();
    }

    @CacheEvict(value = "listaDeTopicos",allEntries = true)
    public ResponseEntity<?> deletar(Long id) {
        Optional<Topico> excluido = topicoRepository.findById(id);
        if (!excluido.isPresent()) {
            return ResponseEntity.notFound().build();
        }
        topicoRepository.delete(excluido.get());
        return ResponseEntity.ok().build();
    }
}
