package br.com.alura.forum.model;

import lombok.*;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
@Setter
@Getter
@Builder
@EqualsAndHashCode
@AllArgsConstructor
public class Resposta {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private LocalDateTime data = LocalDateTime.now();
	private String mensagem;
	private Boolean solucao = false;

	@ManyToOne
	private Topico topico;

	@ManyToOne
	private Usuario usuario;

	public Resposta() {
	}
}
