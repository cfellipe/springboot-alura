package br.com.alura.forum.config.validacao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ValidacaoHandler {

	@Autowired
	private MessageSource messageSource;

	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public ErrosDeValidacaoDto handle(MethodArgumentNotValidException exception) {
		List<ObjectError> globalErrors = exception.getBindingResult().getGlobalErrors();
		List<FieldError> fieldErrors = exception.getBindingResult().getFieldErrors();

		ErrosDeValidacaoDto erros = new ErrosDeValidacaoDto();

		globalErrors.forEach(error -> {
			String mensagem = getMensagemDeErro(error);
			erros.addErroDeRegraDeNegocio(mensagem);
		});

		fieldErrors.forEach(error -> {
			String mensagem = getMensagemDeErro(error);
			erros.addErroDeFormulario(error.getField(), mensagem);
		});

		return erros;
	}

	private String getMensagemDeErro(ObjectError error) {
		return messageSource.getMessage(error, LocaleContextHolder.getLocale());
	}

}
