package br.com.alura.forum.config.validacao;

import java.util.ArrayList;
import java.util.List;

public class ErrosDeValidacaoDto {

	private List<String> errosDeRegraDeNegocio = new ArrayList<>();
	private List<ErroDeFormularioDto> errosDeFormulario = new ArrayList<>();

	public void addErroDeRegraDeNegocio(String erro) {
		errosDeRegraDeNegocio.add(erro);
	}

	public void addErroDeFormulario(String campo, String mensagem) {
		ErroDeFormularioDto erro = new ErroDeFormularioDto(campo, mensagem);
		errosDeFormulario.add(erro);
	}
	
	public List<String> getErrosDeRegraDeNegocio() {
		return errosDeRegraDeNegocio;
	}
	
	public List<ErroDeFormularioDto> getErrosDeFormulario() {
		return errosDeFormulario;
	}

}
