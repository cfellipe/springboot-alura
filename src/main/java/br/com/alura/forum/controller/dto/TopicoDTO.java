package br.com.alura.forum.controller.dto;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import br.com.alura.forum.model.StatusTopico;
import lombok.*;

import javax.persistence.Entity;


@Setter
@Getter
@Builder
@EqualsAndHashCode
@AllArgsConstructor
public class TopicoDTO {

	private Long id;
	private LocalDateTime dataCriacao;
	private String titulo;
	private String mensagem;
	private CursoDTO curso;
	private UsuarioDTO autor;
	private StatusTopico status;
	private List<RespostaDTO> respostas = new ArrayList<>();

	public TopicoDTO() {
	}
}
