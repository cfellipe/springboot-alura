package br.com.alura.forum.controller.dto;

import java.time.LocalDateTime;

import br.com.alura.forum.model.Resposta;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Builder
@AllArgsConstructor
public class RespostaDTO {

	private Long id;
	private LocalDateTime data;
	private String mensagem;
	private Boolean solucao;
	private UsuarioDTO usuario;

	public RespostaDTO() {
	}
}
