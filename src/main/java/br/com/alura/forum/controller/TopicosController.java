package br.com.alura.forum.controller;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.validation.Valid;

import br.com.alura.forum.model.Curso;
import br.com.alura.forum.service.TopicoService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import br.com.alura.forum.controller.dto.TopicoDTO;
import br.com.alura.forum.controller.form.AtualizacaoTopicoForm;
import br.com.alura.forum.controller.form.NovoTopicoForm;
import br.com.alura.forum.model.Topico;
import br.com.alura.forum.model.Usuario;
import br.com.alura.forum.repository.CursoRepository;
import br.com.alura.forum.repository.TopicoRepository;
import br.com.alura.forum.repository.UsuarioRepository;

@RestController
@RequestMapping("/topicos")
public class TopicosController {

	@Autowired
	private TopicoService topicoService;

	@GetMapping
	public Page<TopicoDTO> lista(@RequestParam(required = false) String nomeCurso,
								 @PageableDefault(sort = "id",direction = Sort.Direction.DESC,size = 100) Pageable paginacao) {
		return topicoService.buscaTodos(nomeCurso,paginacao);
	}

	@PostMapping
	public ResponseEntity<TopicoDTO> salvar(@RequestBody @Valid NovoTopicoForm form, UriComponentsBuilder uriBuilder)
			throws URISyntaxException {
		return topicoService.salvarTopico(form,uriBuilder);
	}

	@GetMapping("/{id}")
	public ResponseEntity<TopicoDTO> detalhar(@PathVariable Long id) {
		return topicoService.buscarPorId(id);
	}

	@PutMapping("/{id}")
	public ResponseEntity<?> atualizar(@PathVariable Long id, @RequestBody @Valid AtualizacaoTopicoForm form) {
		return topicoService.atualiza(id,form);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<?> excluir(@PathVariable Long id) {
		return  topicoService.deletar(id);
	}

}
