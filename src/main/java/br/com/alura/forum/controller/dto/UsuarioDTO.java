package br.com.alura.forum.controller.dto;

import lombok.*;

import javax.persistence.Entity;


@Setter
@Getter
@Builder
@EqualsAndHashCode
@AllArgsConstructor
public class UsuarioDTO {
    private String nome;

    public UsuarioDTO() {
    }
}
