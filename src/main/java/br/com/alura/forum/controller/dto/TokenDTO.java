package br.com.alura.forum.controller.dto;


import lombok.*;

@Setter
@Getter
@Builder
@EqualsAndHashCode
@AllArgsConstructor
public class TokenDTO {

    private String token;
    private String tipo;

}
